// v1.0.2

package main

import (
	"fmt"
	lesson_2_packet "gitlab.com/alikhan.murzayev/lesson-2-packet"
)

func main() {
	// usage of first function
	fmt.Println(lesson_2_packet.Concatenation("Alikhan", "Murzayev"))
	// usage of second function to solve quadratic equation
	fmt.Println(lesson_2_packet.QuadraticEquation(1, 1, 0))
	// usage of third function to calculate bitwise shift
	fmt.Println(lesson_2_packet.BitwiseShift(10, 3))
}
